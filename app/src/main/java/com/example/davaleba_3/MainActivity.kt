package com.example.davaleba_3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val items = ArrayList<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        addItemButton.setOnClickListener(){
            items.add(0,ItemModel(R.mipmap.battlefield1, "New Game", "New Description"))
            adapter.notifyItemInserted(0)
            recyclerView.scrollToPosition(0)
        }
        setData()
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(items)
        recyclerView.adapter = adapter
    }

    private fun setData() {
        items.add(ItemModel(R.mipmap.battlefield1, "Battlefield 1", "description1"))
        items.add(ItemModel(R.mipmap.crysis3, "Crysis 3", "description2"))
        items.add(ItemModel(R.mipmap.skyrim, "Skyrim", "description3"))
        items.add(ItemModel(R.mipmap.battlefield4, "Battlefield 4", "description4"))
        items.add(ItemModel(R.mipmap.ghostrecon, "Ghost Recon", "description5"))
        items.add(ItemModel(R.mipmap.revelation, "AC Revelation", "description6"))
        items.add(ItemModel(R.mipmap.sekiro, "Sekiro", "description7"))
        items.add(ItemModel(R.mipmap.battlefield3, "Battlefield 3", "description8"))
    }
}
